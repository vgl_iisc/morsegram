# import modules
import h5py
import itertools
import itk
from matplotlib import pyplot as plt
from numba import jit
import numba
import numpy as np
import os
import re
from scipy import io
import SimpleITK as sitk
from skimage import filters
from skimage.measure import block_reduce
from skimage.segmentation import morphological_chan_vese
import vtk
import vtk.util.numpy_support as nps
import time
import multiproc
from multiprocessing import Process, cpu_count
from tqdm import tqdm
import shutil


def addConnectivityData(dataset):
    """Extract cells that share common points

    Args:
        dataset (vtk polydata): vtk polydata with points and cells
    
    Description:

    Returns:
        vtk polydata: polydata with connectivity
    """
    connectivity_filter = vtk.vtkConnectivityFilter()
    connectivity_filter.SetInputData(dataset)
    connectivity_filter.SetExtractionModeToAllRegions()
    connectivity_filter.ColorRegionsOn()
    connectivity_filter.Update()
    return (connectivity_filter.GetOutput())


@jit(nopython=True)
def adaptive_thresh(image, T=0.15):
    """Adaptive thresholding

    Args:
        image (uint16 np array): two-dimensional image
        T (float, optional): background pixel is less than mean*(1-T).
        Defaults to 0.15.
    
    Description:
        Binarize the grayscale image with adaptive threshold. For each pixel in the image:
        1. Calculate the mean intensity of a window around the pixel
        2. If the pixel intensity is greater than mean intensity: foreground pixel
        3. Otherwise background pixel

        The windowsize is fixed at size of the image / 8.

    Reference:
    Bradley, D. and Roth, G., 2007. Adaptive thresholding using the integral
    image. Journal of graphics tools, 12(2), pp.13-21.

    Returns:
        out_image: thresholded image
    """
    num_row, num_col = image.shape
    win_size = max(num_row, num_col) // 8
    half_win_size = win_size // 2

    # initialize integral image and output image
    int_image = image.copy().astype(numba.uint64)
    out_image = np.zeros((num_row, num_col), dtype=numba.uint8)

    # integral image
    for col in range(num_col):
        temp = 0
        for row in range(num_row):
            temp = temp + image[row, col]
            if col == 0:
                int_image[row, col] = temp
            else:
                int_image[row, col] = int_image[row, col-1] + temp

    for col in range(num_col):
        for row in range(num_row):
            # win_size x win_size region around row and col
            irow = max(row-half_win_size, 0)
            frow = min(row+half_win_size, num_row-1)
            icol = max(col-half_win_size, 0)
            fcol = min(col+half_win_size, num_col-1)

            region_size = (frow-irow)*(fcol-icol)

            sum_ = int_image[frow, fcol]-int_image[irow, fcol] - \
                int_image[frow, icol]+int_image[irow, icol]

            if image[row, col]*region_size <= sum_*(1.0-T):
                out_image[row, col] = 0
            else:
                out_image[row, col] = 255
    return out_image


def bd_extraction(arr, slicewise=True, filterName='InterMode', ace=False,
                  visualize=False):
    """Boundary extraction from image using different filters

    Args:
        arr (np array): attenuation density image
        slicewise (bool, optional): slicewise if true; globally if false. Defaults to True.
        filterName (str, optional): many thresholding filters are available. Defaults to 'InterMode'.
        ace (bool, optional): active countour as boundary surface if true. Defaults to False.
        visualize (bool, optional): visualize the result if true. Defaults to False.
    
    Description:
        Boundary extraction from a gray scale volume image. Different filters can be used:
        1. Huang - Huang
        2. Isodata - Isodata
        3. InterMode - Intermode
        4. Kitt - Kitt 
        5. Li - Li 
        6. MaxEnt - Maximum entropy
        7. Min - Minimum
        8. Moements - Moments 
        9. Otsu - Otsu 
        10. Renyi - Renyi 
        11. Shanbhag - Shanbhag
        12. Triangle - Triangle
        13. Yen - Yen
        14. Adaptive - Adaptive

        Adaptive filter can only be used slice wise.
        

    Returns:
        numpy array: Binary volume after filtering and active contour
    """
    print('Commencing Boundary Extraction')
    # dictionary of filters
    # Minimum filter is from itk
    # Adaptive filter is manually implemented
    filterTypeList = dict(
        Huang=sitk.HuangThresholdImageFilter,
        Isodata=sitk.IsoDataThresholdImageFilter,
        InterMode=sitk.IntermodesThresholdImageFilter,
        Kitt=sitk.KittlerIllingworthThresholdImageFilter,
        Li=sitk.LiThresholdImageFilter,
        MaxEnt=sitk.MaximumEntropyThresholdImageFilter,
        Min=itk.IntermodesThresholdImageFilter,
        Moments=sitk.MomentsThresholdImageFilter,
        Otsu=sitk.OtsuThresholdImageFilter,
        Renyi=sitk.RenyiEntropyThresholdImageFilter,
        Shanbhag=sitk.ShanbhagThresholdImageFilter,
        Triangle=sitk.TriangleThresholdImageFilter,
        Yen=sitk.YenThresholdImageFilter,
        Adaptive=adaptive_thresh,
    )
    filterType = filterTypeList[filterName]

    # slicewise thresholding
    if slicewise:
        ls = np.zeros_like(arr, dtype=np.float32)
        Thresh = np.zeros(arr.shape[0])
        for ii, slice in enumerate(arr):

            if np.max(slice) == 0:
                continue

            # thresholding
            if filterName == 'Min':
                # minimum filter
                filter = filterType.New(itk.GetImageFromArray(slice))
                filter.SetInput(itk.GetImageFromArray(slice))
                filter.UseInterModeOff()
                filter.Update()
                thresh = filter.GetThreshold()

            elif filterName == 'Adaptive':
                # adaptive thresholding
                slice = filterType(slice, T=0.10)
                thresh = 100

            else:
                # sitk filter
                filter = filterType()
                filter.Execute(sitk.GetImageFromArray(slice))
                thresh = filter.GetThreshold()

            Thresh[ii] = thresh

            # active contour
            if ace:
                # active contour
                lambda_1, lambda_2 = 1, 1
                active_contour = morphological_chan_vese(slice, 200, init_level_set=(slice-thresh),
                                                         lambda1=abs(lambda_1), lambda2=abs(lambda_2), smoothing=0)
                ls[ii, :, :] = (active_contour).astype(np.float32)
            else:
                ls[ii, :, :] = (slice > thresh).astype(np.float32)

            if visualize:
                fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(18, 9))
                ax0.imshow(arr[ii, :, :], cmap="Greys")
                ax1.imshow(slice > thresh, cmap="Greys")
                plt.savefig(f'./Dump/slice_{ii}.png', dpi=600)
                plt.close(fig)

        # fill enclosed voids in the binary volume
        fill = sitk.BinaryFillholeImageFilter()
        ls = fill.Execute(sitk.GetImageFromArray(ls.astype(np.ubyte)))
        ls = sitk.GetArrayFromImage(ls)
        np.savetxt('Threshold.txt', Thresh, fmt='%10.4f')

    else:
        # thresholding
        if filterName == 'Min':
            filter = itk.IntermodesThresholdImageFilter.New(
                itk.GetImageFromArray(arr))
            filter.SetInput(itk.GetImageFromArray(arr))
            filter.UseInterModeOff()
            filter.Update()
            thresh = filter.GetThreshold()

        elif filterName == 'Adaptive':
            print('Adaptive thresholding is not available for 3d')
            raise NotImplementedError

        else:
            filter = filterType()
            filter.Execute(sitk.GetImageFromArray(arr))
            thresh = filter.GetThreshold()
            print("The threshold is: ", thresh)

        # active contour
        if ace:
            # active contour
            lambda_1, lambda_2 = 1, 1
            active_contour = (morphological_chan_vese(arr, 200,
                                                      init_level_set=(
                                                          arr-thresh),
                                                      lambda1=abs(lambda_1), lambda2=abs(lambda_2), smoothing=0))
            ls = (active_contour).astype(np.float32)
        else:
            ls = (arr > thresh).astype(np.float32)

    print('Boundary Extracted.')
    return ls


def bimode_log_min(vols):
    """Automatic thresholding -- volume cuoff

    Args:
        vols (list): list of volume of particles
    
    Description:
        For removing low volume noises in segmentation. 
        Histogram for log of particle volumes from segmentation is usually a bimodal distribution. 
        Choose the intermode value to remove low volume noise.

    Returns:
        float: volume cutoff
    """
    plt.figure()
    plt.hist(np.log(vols))
    plt.show()
    return np.exp(filters.threshold_otsu(np.log(vols)))


def check_segmentation(raw_file_name, centers):
    """Write the centers on slices

    Args:
        raw_file_name (str): raw file with original data
        centers (list): list of particle centers
    
    Description:
        This function is for  checking the segmentation accuracy. The raw file is read into a volume
        array. Intensity of window region around center is edited to 0 so that it becomes clearly visible
        in the slices. The slices of whole volume is written for checking. 

    Returns:
        None: None
    """
    reader = sitk.ImageFileReader()
    reader.SetImageIO("MetaImageIO")
    reader.SetFileName(raw_file_name)
    arr = sitk.GetArrayFromImage(reader.Execute())
    arr = arr.transpose(2, 1, 0)
    # arrmin, arrmax = np.min(arr), np.max(arr)

    s = 2
    for center in centers:
        center = center.astype(int)
        try:
            arr[center[0]-s:center[0]+s, center[1]-s:center[1] +
                s, center[2]-s:center[2]+s] = 0
        except:
            arr[center[0]-s:center[0]+s, center[1] -
                s:center[1]+s, center[2]] = 0

    for ii in range(arr.shape[2]):
        slice = arr[:, :, ii]
        # slice = arrmin + (arrmax - arrmin) * slice / (np.max(slice) - np.min(slice))
        fig, ax = plt.subplots(1, 1)
        ax.imshow(slice, interpolation="none", cmap="Greys_r")
        plt.savefig(f'./Dump/check_slice_{ii:04d}')
        plt.close(fig)

    return None


def collect_neighbours(points):
    """This function collects the 8-neighbours for midpoints in a grid

    Args:
        points (np array): array of midpoints in a grid
    
    Description:

    Returns:
        np array: 8-neighbour grid points
    """
    inc = np.array(list(itertools.product((-0.5, 0.5), repeat=3)))
    pts = inc.reshape(1, inc.shape[1], inc.shape[0])\
        + points.reshape(points.shape[0], points.shape[1], 1)
    pts = pts.transpose([0, 2, 1]).reshape(-1, 3)
    return np.unique(pts.astype(int), axis=0)


def compute_contact_regions(msc, image, isDesManifold=True):
    """Get the contact regions

    Args:
        msc (msc object): Morse Complex object
        image (np aray): distance field
    
    Description:

    Returns:
        vtk polydata: vtkpolydata with contact points and cells
    """
    # get the contact region -- descending manifold of 2-saddle
    msc.collect_geom(dim=2, dir=0)
    # ''' critical points type
    # dim: Critical point type \n"\
    #     "   dim=-1      --> All (default)\n"\
    #     "   dim=0,1,2,3 --> Minima, 1-saddle,2-saddle,Maxima \n"\
    # dir: Geometry type \n"\
    #     "   dir=0 --> Descending \n"\
    #     "   dir=1 --> Ascending \n"\
    #     "   dir=2 --> Both (default) \n"\
    # '''

    # get the coordinates of primal points
    primal_pts = msc.primal_points()

    # get the 2 saddle points
    cps_2sad = msc.cps(2)

    # initialize vtk data type
    cp_ids = vtk.vtkIntArray()
    cp_ids.SetName('CP ID')
    val = vtk.vtkFloatArray()
    val.SetName('Val')
    des_man_pts = vtk.vtkPoints()
    des_man_pts.SetData(nps.numpy_to_vtk(primal_pts, "Pts"))
    des_man_quads = vtk.vtkCellArray()

    start_time = time.time()

    num_proc = cpu_count()
    print("Number of processors: ", num_proc)

    proc_works = [[] for i in range(num_proc)]
    for i, m in enumerate(cps_2sad):
        proc_works[i % num_proc].append(m)
    
    list_procs = []

    # create a folder for saddle
    saddle2_folder = '../Outputs/Saddle2'

    if os.path.exists(saddle2_folder):
        shutil.rmtree(saddle2_folder)

    os.makedirs(saddle2_folder)
    

    for i in range(num_proc):
        l_p = Process(target=multiproc.contact_region_task, args=(i, saddle2_folder, 
                            proc_works[i], msc, primal_pts, image, isDesManifold))
        list_procs.append(l_p)
        l_p.start()
        print("Started process ", i)
    
    for l_p in list_procs:
        l_p.join()
        print("Joined process ", l_p.pid)

    # terminate all the processes
    for l_p in list_procs:
        l_p.terminate()
        print("Terminated process ", l_p.pid)

    surv_sads = []

    # iterate over all the files and collect the results:
    for i in range(num_proc):
        p_surv_sads = np.load(saddle2_folder + "/surv_sads_%d.npy" % i)
        p_des_man_quads = np.load(saddle2_folder + "/des_man_quads_%d.npy" % i)
        p_cp_ids = np.load(saddle2_folder + "/cp_ids_%d.npy" % i)
        surv_sads.extend(p_surv_sads)
        for cid, quad in zip(p_cp_ids, p_des_man_quads):
            cp_ids.InsertNextValue(int(cid))
            des_man_quads.InsertNextCell(4)
            des_man_quads.InsertCellPoint(quad[0])
            des_man_quads.InsertCellPoint(quad[1])
            des_man_quads.InsertCellPoint(quad[3])
            des_man_quads.InsertCellPoint(quad[2])
    
    # stores the descending manifolds
    des_man = None
    if isDesManifold:
        des_man = vtk.vtkPolyData()
        des_man.SetPoints(des_man_pts)
        des_man.SetPolys(des_man_quads)
        des_man.GetCellData().AddArray(cp_ids)
        # des_man.GetPointData().AddArray(val)
        des_man = addConnectivityData(des_man)
        # des_man, surv_sads = extract_surviving_sads(des_man, msc)
    return des_man, surv_sads


def dist_field_comp(ls):
    """Get the chamfer distance field from the binary volume

    Args:
        ls (numpy array): binary volume array
    
    Description:

    Returns:
        numpy array: distance field as numpy array
    """
    print('Commencing Distance Field Computation')
    itk_image = itk.GetImageFromArray(ls.astype(np.float32))

    antialiasfilter = itk.AntiAliasBinaryImageFilter.New(itk_image)
    antialiasfilter.SetInput(itk_image)
    antialiasfilter.Update()
    antialias_image = antialiasfilter.GetOutput()

    isoContourFilter = itk.IsoContourDistanceImageFilter.New(antialias_image)
    isoContourFilter.SetLevelSetValue(0.5)
    isoContourFilter.SetFarValue(100)
    isoContourFilter.SetInput(antialias_image)
    isoContourFilter.Update()
    isoContour_image = isoContourFilter.GetOutput()

    chamferFilter = itk.FastChamferDistanceImageFilter.New(isoContour_image)
    chamferFilter.SetMaximumDistance(50.0)
    chamferFilter.Update()
    chamferFilter.SetInput(isoContour_image)
    chamferFilter.Update()
    chamf_image = chamferFilter.GetOutput()
    dist_field = itk.GetArrayFromImage(chamf_image)
    print('Distance Field Computed')
    return dist_field.astype(np.float32)


def extract_surviving_sads(des_man, msc):
    """Extract surviving saddles from descending manifold after contact computation

    Args:
        des_man (vtk polydata): contact regions
        msc (msc object): Morse Complex object
    
    Description:

    Returns:
        list: list of surviving saddles indices
    """
    # region_val_dict = {}
    # region_cp_dict = {}
    # num_cells = des_man.GetNumberOfCells()
    # regions = des_man.GetCellData().GetArray('RegionId')
    # cp_ids = des_man.GetCellData().GetArray('CP ID')
    # for i in range(num_cells):
    #     region_id = regions.GetTuple1(i)
    #     cp_id = cp_ids.GetTuple1(i)
    #     cp_val = msc.cp_func(int(cp_id))
    #     if(region_id not in region_val_dict.keys()):
    #         region_val_dict[region_id] = cp_val
    #         region_cp_dict[region_id] = cp_id
    #     else:
    #         if(cp_val > region_val_dict[region_id]):
    #             region_val_dict[region_id] = cp_val
    #             region_cp_dict[region_id] = cp_id
    # for i in range(num_cells):
    #     region_id = regions.GetTuple1(i)
    #     cp_ids.SetTuple1(i, region_cp_dict[region_id])
    # return des_man, list(set(region_cp_dict.values()))
    pass


def get_dims(filename):
    """This function extracts the DimSize as tuple from the mhd file.

    Args:
        filename (str): mhd file with the dimensions of 3-d volume
    
    Description:

    Returns:
        tuple: dimensions
    """
    path, file = os.path.split(filename)
    filename = os.path.join(path, os.path.splitext(file)[0]+'.mhd')
    with open(filename, mode='r') as f:
        text = f.read().split('\n')

    for line in text:
        if re.search('DimSize', line):
            txt = line.split(' ')
            dims = (int(txt[2]), int(txt[3]), int(txt[4]))
    return dims


def get_extremum_graph(msc, surviving_sads):
    """Get the extremum graph of surviving saddles

    Args:
        msc (msc object): Morse Smale object
        surviving_sads (list): list of surviving saddle indices
    
    Description:

    Returns:
        vtk polydata: vtk polydata with connectivity network
    """
    '''
    Collect the geometry of all survivng critical points
        Parameters:
            dir: Geometry type
            dir=0 --> Descending
            dir=1 --> Ascending
            dir=2 --> Both (default)
            dim: Critical point type
            dim=-1 --> All (default)
            dim=0,1,2,3 --> Minima, 1-saddle,2-saddle,Maxima \n"\
    '''
    # Ascending manifold of 2-saddle
    msc.collect_geom(dim=2, dir=1)
    # coordinates of critical points
    dp = msc.dual_points()

    pa, ia = vtk.vtkPoints(), vtk.vtkIntArray()
    fa, ca = vtk.vtkFloatArray(), vtk.vtkCellArray()
    pa.SetData(nps.numpy_to_vtk(dp, "Pts"))
    ia.SetName("SaddleIndex")
    fa.SetName("SaddleVal")
    for s in surviving_sads:
        s = int(s)
        # collect the ascending geom
        gm = msc.asc_geom(s)
        for a, b in gm:
            ca.InsertNextCell(2)
            ca.InsertCellPoint(a)
            ca.InsertCellPoint(b)
            ia.InsertNextValue(s)
            fa.InsertNextValue(msc.cp_func(s))
    pd = vtk.vtkPolyData()
    pd.SetPoints(pa)
    pd.SetLines(ca)
    pd.GetCellData().AddArray(ia)
    pd.GetCellData().AddArray(fa)
    return pd


def get_cp(msc, cp_type):
    """Get the information about the critical points

    Args:
        msc (msc objec): Morse Complex object
        cp_type (int): critical point type
                        0 - minima
                        1 - 1-saddle
                        2 - 2-saddle
                        3 - maxima

    Description:

    Returns:
        vtk polydata: coordinates, index type, function value, index
    """
    req_cps = msc.cps(cp_type)
    cpList = []
    for m in req_cps:
        if(msc.cp_func(m) > 0):
            cpList.append((msc.cp_cellid(m), 3, msc.cp_func(m), m))
    cpList = list(set(cpList))

    # create the vtk objects
    pa, ia = vtk.vtkPoints(), vtk.vtkIntArray()
    fa, cp_ids = vtk.vtkFloatArray(), vtk.vtkIntArray()
    ia.SetName("Index")
    fa.SetName("Val")
    cp_ids.SetName("CP ID")

    ca = vtk.vtkCellArray()
    for i, (p, idx, val, cpidx) in enumerate(cpList):
        pa.InsertNextPoint(np.array(p, np.float32)/2)
        ia.InsertNextValue(idx)
        fa.InsertNextValue(val)
        cp_ids.InsertNextValue(cpidx)
        ca.InsertNextCell(1)
        ca.InsertCellPoint(i)

    # Set the outputs
    pd = vtk.vtkPolyData()
    pd.SetPoints(pa)
    pd.SetVerts(ca)
    pd.GetPointData().AddArray(ia)
    pd.GetPointData().AddArray(fa)
    pd.GetPointData().AddArray(cp_ids)
    return pd


def get_saddles(msc, surv_sads):
    """From indices of surviving saddles get further information

    Args:
        msc (msc object): Morse complex object
        surv_sads (list): indices of saddle points that survived
    
    Description:

    Returns:
        vtk polydata: coords, index type, function at the saddle points, saddle index, max1, max2
    """
    cpList = []
    for s in tqdm(surv_sads):
        s, index = int(s), 2  # saddle index - 2
        # maxList is the max critical points connected with s
        co_ords, val, maxList = msc.cp_cellid(
            s), msc.cp_func(s), msc.asc(s)[:, 0]
        cpList.append((co_ords, index, val, s, maxList[0], maxList[1], 
                       msc.cp_func(maxList[0]), msc.cp_func(maxList[1])))

    # cpList = list(set(cpList))

    # create the vtk objects
    # (pa => coords(), ia => index, fa => val)
    pa, ia = vtk.vtkPoints(), vtk.vtkIntArray()
    fa, cp_ids = vtk.vtkFloatArray(), vtk.vtkIntArray()
    max_1, max_2 = vtk.vtkIntArray(), vtk.vtkIntArray()
    max_1_values, max_2_values = vtk.vtkFloatArray(), vtk.vtkFloatArray()
    ia.SetName("Index")
    fa.SetName("Val")
    cp_ids.SetName("CP ID")
    max_1.SetName("Max 1")
    max_2.SetName("Max 2")
    max_1_values.SetName("Max 1 Val")
    max_2_values.SetName("Max 2 Val")
    maxs = []

    ca = vtk.vtkCellArray()
    for i, (p, idx, val, cpidx, m1idx, m2idx, m1val, m2val) in tqdm(enumerate(cpList)):
        pa.InsertNextPoint(np.array(p, np.float32)/2)
        ia.InsertNextValue(idx)
        fa.InsertNextValue(val)
        max_1.InsertNextValue(m1idx)
        max_2.InsertNextValue(m2idx)
        max_1_values.InsertNextValue(m1val)
        max_2_values.InsertNextValue(m2val)
        cp_ids.InsertNextValue(cpidx)
        ca.InsertNextCell(1)
        ca.InsertCellPoint(i)
        maxs.append(m1idx)
        maxs.append(m2idx)

    # Set the outputs
    pd = vtk.vtkPolyData()
    pd.SetPoints(pa)
    pd.SetVerts(ca)
    pd.GetPointData().AddArray(ia)
    pd.GetPointData().AddArray(fa)
    pd.GetPointData().AddArray(cp_ids)
    pd.GetPointData().AddArray(max_1)
    pd.GetPointData().AddArray(max_2)
    pd.GetPointData().AddArray(max_1_values)
    pd.GetPointData().AddArray(max_2_values)
    return pd, maxs


def get_segmentation_index_dual(msc, img, rtype="VTP"):
    """Get the segmentation from morse smale complex

    Args:
        msc (msc object): Morse Complex object
        img (np array): Distance field
        rtype (str, optional): "VTP" or "NP". Defaults to "VTP".
    
    Description:

    Raises:
        TypeError: "VTP" and "NP" are allowed as output

    Returns:
        np array: numpy array as segmentation or vtp as segmenation
    """
    # descending manifold of maxima
    msc.collect_geom(dim=3, dir=0)
    # ''' critical points type
    # dim: Critical point type \n"\
    #     "   dim=-1      --> All (default)\n"\
    #     "   dim=0,1,2,3 --> Minima, 1-saddle,2-saddle,Maxima \n"\
    # dir: Geometry type \n"\
    #     "   dir=0 --> Descending \n"\
    #     "   dir=1 --> Ascending \n"\
    #     "   dir=2 --> Both (default) \n"\
    # '''

    # point coordinates
    dp = msc.dual_points()
    cps_max = msc.cps(3)
    if rtype == "VTP":
        
        ensem_dir = "../Outputs/grains/"

        for dir in [ensem_dir]:
            if not os.path.exists(dir):
                os.mkdir(dir)

        start_time = time.time()

        num_proc = cpu_count()
        print("Number of processors: ", num_proc)

        proc_works = [[] for i in range(num_proc)]
        for i, m in enumerate(cps_max):
            proc_works[i % num_proc].append(m)
        
        list_procs = []

        for i in range(num_proc):
            l_p = Process(target=multiproc.proc_work, args=(proc_works[i], msc, dp, img, ensem_dir))
            list_procs.append(l_p)
            l_p.start()
            print("Started process ", i)
        
        for l_p in list_procs:
            l_p.join()
            print("Joined process ", l_p.pid)

        poly_data = vtk.vtkPolyData()

        pa, cp_ids = vtk.vtkPoints(), vtk.vtkIntArray()
        ca, val = vtk.vtkCellArray(), vtk.vtkFloatArray()
        cp_ids.SetName("CP ID")
        val.SetName('Distance Val')
        count = 0

        print("Merging files from: ", ensem_dir)

        for grain_pd_file in tqdm(os.listdir(ensem_dir)):

            # grain_cp_id.vtp

            reader = vtk.vtkXMLPolyDataReader()
            reader.SetFileName(ensem_dir + grain_pd_file)
            # print("Reading file: ", ensem_dir + grain_pd_file)
            reader.Update()
            grain_pd = reader.GetOutput()

            for i in range(grain_pd.GetNumberOfPoints()):
                pa.InsertNextPoint(grain_pd.GetPoint(i))
                cp_ids.InsertNextValue(grain_pd.GetPointData().GetArray("CP ID").GetValue(i))
                val.InsertNextValue(grain_pd.GetPointData().GetArray("Distance Val").GetValue(i))
                ca.InsertNextCell(1)
                ca.InsertCellPoint(count)
                count += 1
            
        poly_data.SetPoints(pa)
        poly_data.SetVerts(ca)
        poly_data.GetPointData().AddArray(cp_ids)
        poly_data.GetPointData().AddArray(val)

        print("Time taken for segmentation: ", time.time() - start_time, " seconds")
        return poly_data


    elif rtype == "NP":
        count = 0
        seg_img = np.full(img.shape, 0, dtype=np.uint32, order='C')
        centers, maxima, labs, vols = [], [], [], []
        for m in cps_max:
            if (msc.cp_func(m) <= 0):
                continue
            des_geom = msc.des_geom(m)

            # we loose information by converting to int <--- issure here
            points = surv_voxs(dp[des_geom].astype(int), img)
            if len(points) == 0:
                pts = collect_neighbours(dp[des_geom])
                points = surv_voxs(pts, img)
                if len(points) == 0:
                    continue
            points_ind = np.ravel_multi_index(points.transpose(), img.shape)
            np.put(seg_img, points_ind, m)
            centers.append(np.mean(points, axis=0))
            maxima.append(np.array(msc.cp_cellid(m), dtype=np.float)/2)
            labs.append(m)
            vols.append(points.shape[0])
            count += 1
        print(f'Number of particles segmented: {count}')
        centers, maxima, labs, vols = np.array(centers), np.array(
            maxima), np.array(labs), np.array(vols)
        return seg_img, centers, maxima, labs, vols
    else:
        raise TypeError("Return type not configured.")
    return None


def read_input_file(filename, factor=1):
    """Read and downsample mat/raw file into numpy array

    Args:
        filename (str): Name of raw / mat file with volm array
        factor (int, optional): Downsampling factor. Defaults to 1.
    
    Description:

    Raises:
        Exception: Not able to read mat file

    Returns:
        numpy array: downsampled array
    """
    file_ext = filename.split(".")[-1]
    if file_ext == "mat":
        print("reading mat file : " + filename)
        try:
            # if saved wih -v7.3 flag
            f = h5py.File(f'{filename}', 'r')
            sci = False
        except OSError:
            # else
            f = io.loadmat(f'{filename}')
            sci = True
        except:
            print("Something went wrong")
            raise Exception('Not able to read the mat file.')

        data = f.get('volm')
        arr = np.array(data)
        if sci:
            arr = np.transpose(arr, axes=[2, 1, 0])
    elif file_ext == "mhd":
        print("reading raw file : " + filename)
        file_reader = sitk.ImageFileReader()
        file_reader.SetFileName(filename)
        file_reader.SetImageIO('')
        image = file_reader.Execute()
        arr = sitk.GetArrayFromImage(image)
    print("Original Shape : ", arr.shape)
    if (factor > 1):
        arr = block_reduce(arr, (factor, factor, factor),
                           func=np.mean).astype(np.uint16)
    print("New Shape : ", arr.shape)
    return arr


def read_msc_to_img(msc, dim):
    """This function extracts the Morse Smale complex vertices data to a numpy array.

    Args:
        msc (msc): morse smale complex object
        dim (tuple): dimensions of the image
    
    Description:

    Returns:
        np array: numpy array of the function value at msc vertices
    """
    np_arr = np.zeros(dim)
    for x in range(dim[0]):
        for y in range(dim[1]):
            for z in range(dim[2]):
                # scalar value at the vertex coordinate
                np_arr[x, y, z] = msc.vert_func(x, y, z)
    return np_arr  # flatten order - F


def surv_voxs(points, img):
    """This function removes the voxels in background from the list of voxels.

    Args:
        points (np array): array of voxel coordinates
        img (np array): image
    
    Description:

    Returns:
        np array: surviving voxel coordinates
    """
    points_ind = np.ravel_multi_index(points.transpose(), img.shape)
    points_val = img.ravel()[points_ind]
    points = points[points_val > 0, ...]
    return points